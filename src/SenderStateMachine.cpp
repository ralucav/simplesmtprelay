
#include <syslog.h>
#include <unistd.h>

#include "SenderStateMachine.h"


//----------------------------------------------------------------------
SenderStateMachine::SenderStateMachine() {
	reset();
}
//----------------------------------------------------------------------
void SenderStateMachine::reset() {
	_state = INITIAL;
}
//----------------------------------------------------------------------
SenderStateMachine::~SenderStateMachine() {
	// TODO Auto-generated destructor stub
}
//----------------------------------------------------------------------
void SenderStateMachine::advanceState(const SmtpReply& reply)
{
	if(!reply.isSuccessOrIntermediate()) {
		syslog(LOG_WARNING | LOG_USER, "Reply %d from server!", reply.getCode());
		_state = ERROR;
		return;
	}

	switch(_state) {
	case INITIAL:
		if(reply.isWelcome()) {
			_state = HELO; return;
		}
		break;
	case HELO:
		if(reply.isActionCompleted()) {
			_state = MAIL; return;
		}
		break;
	case MAIL:
		if(reply.isActionCompleted()) {
			_state = RCPT; return;
		}
		break;
	case RCPT:
		if(reply.isActionCompleted()) {
			_state = DATA; return;
		}
		break;
	case DATA:
		if(reply.isIntermediateMailData()) {
			_state = MAIL_DATA; return;
		}
		break;
	case MAIL_DATA:
		if(reply.isActionCompleted()) {
			_state = END_OF_SESSION; return;
		}
		break;
	case END_OF_SESSION:
		if(reply.isEndOfSession()) {
			_state = TERMINATED; return;
		}
		break;
	default:
		break;

	}
	_state = ERROR;
}
//----------------------------------------------------------------------


