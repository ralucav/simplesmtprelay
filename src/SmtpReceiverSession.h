
#ifndef SMTPSERVERSESSION_H_
#define SMTPRECEIVERSESSION_H_

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>

#include "ReceiverStateMachine.h"
#include "SmtpReceiverTransaction.h"

using boost::asio::ip::tcp;

class SmtpReceiverSession;
typedef boost::shared_ptr<SmtpReceiverSession> SmtpReceiverSessionPtr;

class SmtpReceiverSession
		: public boost::enable_shared_from_this<SmtpReceiverSession>
{
public:
	static SmtpReceiverSessionPtr create(boost::asio::io_service& ioService);

	virtual ~SmtpReceiverSession();

	tcp::socket& getSocket();
	void start();

private:
	SmtpReceiverSession(boost::asio::io_service& ioService);

	void onSimpleCommand(const boost::system::error_code& error, size_t bytesTransferred);
	void onMailData(const boost::system::error_code& error, size_t bytesTransferred);
	void onWrite(const boost::system::error_code& error);
	void sendReply(SmtpReply& reply);


	void sendWelcomeMessage();
	void waitForData();

	bool isEndOfSession() const {
		return _stateMachine.isEndOfSession();
	}

	bool isMailData() const {
		return _stateMachine.isMailData();
	}

	bool beginTransaction(const SmtpCommand& command, SmtpReply& reply) const;
	bool addCommandToTransaction(const SmtpCommand& command, const SmtpReply& reply) const;

private:
	tcp::socket _socket;
	boost::asio::io_service::strand _strand;

	boost::asio::streambuf _request;

	ReceiverStateMachine _stateMachine;
	SmtpReceiverTransactionPtr _transaction;
	SmtpCommand _command;
};

#endif /* SMTPSERVERSESSION_H_ */
