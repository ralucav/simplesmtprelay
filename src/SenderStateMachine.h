
#ifndef SENDERSTATEMACHINE_H_
#define SENDERSTATEMACHINE_H_

#include "SmtpReply.h"

class SenderStateMachine
{
	typedef enum {
		INITIAL = 0,
		HELO,
		MAIL,
		RCPT,
		DATA,
		MAIL_DATA,
		END_OF_SESSION,
		TERMINATED,
		ERROR
	} SenderState;
public:
	SenderStateMachine();
	virtual ~SenderStateMachine();

	bool isInitial() const { return _state == INITIAL; }
	bool isHelo() const { return _state == HELO; }
	bool isMail() const { return _state == MAIL; }
	bool isRcpt() const { return _state == RCPT; }
	bool isData() const { return _state == DATA; }
	bool isMailData() const { return _state == MAIL_DATA; }
	bool isError() const { return _state == ERROR; }
	bool isEndOfSession() const {return _state == END_OF_SESSION; }
	bool isTerminated() const {return _state == TERMINATED; }

	void advanceState(const SmtpReply& reply);
	void reset();

private:
	SenderState _state;
};

#endif /* SENDERSTATEMACHINE_H_ */
