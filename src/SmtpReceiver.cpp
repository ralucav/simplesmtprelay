
#include <boost/bind.hpp>
#include "SmtpReceiver.h"

//-------------------------------------------------------------
SmtpReceiver::SmtpReceiver(boost::asio::io_service& ioService)
	: _ioService(ioService),
	  _acceptor(ioService, tcp::endpoint(tcp::v4(), 25))
{
	startAccept();
}
//-------------------------------------------------------------
void SmtpReceiver::startAccept()
{
	SmtpReceiverSessionPtr smtpSession = SmtpReceiverSession::create(_ioService);
	_acceptor.async_accept(
		smtpSession->getSocket(),
		boost::bind(
			&SmtpReceiver::onAccept,
			this,
			smtpSession,
			boost::asio::placeholders::error)
	);
}
//-------------------------------------------------------------
void SmtpReceiver::onAccept(SmtpReceiverSessionPtr smtpSession, const boost::system::error_code& error)
{
	if(!error) {
		smtpSession->start();
	}
	startAccept();
}
//-------------------------------------------------------------
