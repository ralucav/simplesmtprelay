
#ifndef SMTPCONSTANTS_H_
#define SMTPCONSTANTS_H_

#include <string>

class SmtpConstants
{
public:

	static const std::string DELIMITER;
	static const std::string MAIL_DATA_DELIMITER;

	virtual ~SmtpConstants();

private:
	SmtpConstants();
};

#endif /* SMTPCONSTANTS_H_ */
