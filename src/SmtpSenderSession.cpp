
#include <iostream>
#include <istream>
#include <sstream>

#include <syslog.h>
#include <boost/bind.hpp>

#include "SmtpSenderSession.h"
#include "SmtpSenderTransaction.h"
#include "MxLookup.h"
#include "SmtpConstants.h"

//----------------------------------------------------------------------
SmtpSenderSession::SmtpSenderSession(boost::asio::io_service& ioService, SmtpSenderTransactionPtr transaction)
	: _socket(ioService), _resolver(ioService), _strand(ioService), _transaction(transaction)
{
}
//----------------------------------------------------------------------
SmtpSenderSession::~SmtpSenderSession() {
}
//----------------------------------------------------------------------
SmtpSenderSessionPtr SmtpSenderSession::create(boost::asio::io_service& ioService, SmtpSenderTransactionPtr transaction)
{
	return SmtpSenderSessionPtr(new SmtpSenderSession(ioService, transaction));
}
//----------------------------------------------------------------------
void SmtpSenderSession::start()
{
	syslog(LOG_INFO, "SmtpSenderSession started");
	_mxCandidates = MxLookup::lookup(_transaction->getRecipientDomain());
	tryNextMxCandidate();
}
//----------------------------------------------------------------------
void SmtpSenderSession::tryNextMxCandidate()
{
	if(!_mxCandidates.empty()) {
		std::string mxCandidate = _mxCandidates.front();
		_mxCandidates.pop_front();
		syslog(LOG_INFO, "Trying candidate %s", mxCandidate.c_str());

		_stateMachine.reset();

		tcp::resolver::query query(mxCandidate, "smtp");
		_resolver.async_resolve(
				query,
				_strand.wrap(boost::bind(
						&SmtpSenderSession::onResolve,
						shared_from_this(),
						boost::asio::placeholders::error,
						boost::asio::placeholders::iterator))
		);
	} else {
		syslog(LOG_INFO, "No more mx candidates to try");
	}
}
//----------------------------------------------------------------------
void SmtpSenderSession::onResolve(const boost::system::error_code& error, tcp::resolver::iterator endpointIterator)
{
	if(!error) {
		boost::asio::async_connect(_socket, endpointIterator,
				_strand.wrap(boost::bind(
						&SmtpSenderSession::onConnect,
						shared_from_this(),
						boost::asio::placeholders::error))
		);
	} else {
		syslog(LOG_WARNING, "Sender error on resolve: %s", error.message().c_str());
		tryNextMxCandidate();
	}
}
//----------------------------------------------------------------------
void SmtpSenderSession::onConnect(const boost::system::error_code& error)
{
	if(!error) {
		waitForReply();
	} else {
		syslog(LOG_WARNING, "Sender error on connect: %s", error.message().c_str());
		tryNextMxCandidate();
	}
}
//----------------------------------------------------------------------
void SmtpSenderSession::onRead(const boost::system::error_code& error)
{
	if(!error) {

		std::istream replyStream (&_reply);
		unsigned int code;
		replyStream >> code;

		std::string replyContent;
		std::getline(replyStream, replyContent);

		syslog(LOG_INFO, "Receive reply %d %s", code, replyContent.c_str());

		SmtpReply reply (code, replyContent);
		_stateMachine.advanceState(reply);

		if( isError() ) {
			tryNextMxCandidate();
		} else if( !isTerminated() ) {
			std::string command = getNextCommand();

			syslog(LOG_INFO, "Send command [%s]", command.c_str());

			boost::asio::async_write(_socket, boost::asio::buffer(command),
				_strand.wrap(
					boost::bind(
						&SmtpSenderSession::onWrite,
						shared_from_this(),
						boost::asio::placeholders::error))
			);
		}
	} else {
		syslog(LOG_WARNING, "Sender error on read: %s", error.message().c_str());
	}
}
//----------------------------------------------------------------------
// THIS NEEDS TO BE REFACTORED
std::string SmtpSenderSession::getHelloCommand() const
{
	std::stringstream commandStream;
	commandStream << "HELO" << " ";
	commandStream << "localhost.localdomain";
	commandStream << SmtpConstants::DELIMITER;
	return commandStream.str();
}
//----------------------------------------------------------------------
std::string SmtpSenderSession::getMailCommand() const
{
	std::stringstream commandStream;
	commandStream << "MAIL FROM:<";
	commandStream << _transaction->getMailFrom() << ">";
	commandStream << SmtpConstants::DELIMITER;
	return commandStream.str();
}
//----------------------------------------------------------------------
std::string SmtpSenderSession::getRcptCommand() const
{
	std::stringstream commandStream;
	commandStream << "RCPT TO:<";
	commandStream << _transaction->getRecipient() << ">";
	commandStream << SmtpConstants::DELIMITER;
	return commandStream.str();
}
//----------------------------------------------------------------------
std::string SmtpSenderSession::getDataCommand() const
{
	std::stringstream commandStream;
	commandStream << "DATA";
	commandStream << SmtpConstants::DELIMITER;
	return commandStream.str();
}
//----------------------------------------------------------------------
std::string SmtpSenderSession::getMailDataCommand() const
{
	// mail data already contais the delimiter
	std::stringstream commandStream;
	commandStream << _transaction->getMailData();
	return commandStream.str();
}
//----------------------------------------------------------------------
std::string SmtpSenderSession::getEndOfSessionCommand() const
{
	std::stringstream commandStream;
	commandStream << "QUIT";
	commandStream << SmtpConstants::DELIMITER;
	return commandStream.str();
}
//----------------------------------------------------------------------
std::string SmtpSenderSession::getNextCommand()
{
	if(_stateMachine.isHelo()) return getHelloCommand();
	if(_stateMachine.isMail()) return getMailCommand();
	if(_stateMachine.isRcpt()) return getRcptCommand();
	if(_stateMachine.isData()) return getDataCommand();
	if(_stateMachine.isMailData()) return getMailDataCommand();
	if(_stateMachine.isEndOfSession()) return getEndOfSessionCommand();
	return std::string("");
}
//----------------------------------------------------------------------
void SmtpSenderSession::onWrite(const boost::system::error_code& error)
{
	if (!error) {
		waitForReply();
	} else {
		syslog(LOG_WARNING, "Sender error on write: %s", error.message().c_str());
	}
}
//----------------------------------------------------------------------
void SmtpSenderSession::waitForReply()
{
	boost::asio::async_read_until(_socket, _reply, SmtpConstants::DELIMITER,
			_strand.wrap(
				boost::bind(
					&SmtpSenderSession::onRead,
					shared_from_this(),
					boost::asio::placeholders::error))
	);
}
//----------------------------------------------------------------------
bool SmtpSenderSession::isTerminated() const
{
	return _stateMachine.isTerminated();
}
//----------------------------------------------------------------------
bool SmtpSenderSession::isError() const
{
	return _stateMachine.isError();
}
//----------------------------------------------------------------------


