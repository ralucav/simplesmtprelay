
#ifndef SMTPCLIENT_H_
#define SMTPSENDER_H_

#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "SmtpSenderTransaction.h"

class SmtpSender
{
public:
	SmtpSender(boost::asio::io_service& ioService);
	virtual ~SmtpSender();

	void processTransactionQueue();

private:
	/**
	 * @brief Creates a list of sender transactions from a receiver transaction, each sender transaction
	 * 		  contains only one recipient field while a receiver transaction may contain more than one
	 * 		  recipient field
	 * @param receiverTransaction
	 * @return list of sender transactions
	 */
	SmtpSenderTransactions getSenderTransactions(SmtpReceiverTransactionPtr receiverTransaction);

	std::string getAddress(const std::string& commandContent);

	void printSenderTransactions(const SmtpSenderTransactions& senderTransactions);

private:
	boost::thread _internalThread;
	boost::asio::io_service& _ioService;
};

#endif /* SMTPCLIENT_H_ */
