
#include "SmtpReply.h"
#include "SmtpReplies.h"

//----------------------------------------------------------------------
SmtpReply::SmtpReply(unsigned short int code, const std::string& content)
	: _code(code), _content(content)
{
}
//----------------------------------------------------------------------
SmtpReply::SmtpReply()
{
	_code = 0;
}
//----------------------------------------------------------------------
SmtpReply::SmtpReply(unsigned short int code)
	: _code(code)
{
}
//----------------------------------------------------------------------
SmtpReply::SmtpReply(const SmtpReply& other)
{
	_code = other._code;
	_content = other._content;
}
//----------------------------------------------------------------------
bool SmtpReply::isActionCompleted() const
{
	// grrr...
	return _code == 250;
}
//----------------------------------------------------------------------
bool SmtpReply::isSuccessOrIntermediate() const
{
	return isSuccess() || isIntermediate();
}
//----------------------------------------------------------------------
bool SmtpReply::isSuccess() const
{
	return _code >= 200 && _code <= 299;
}
//----------------------------------------------------------------------
bool SmtpReply::isIntermediate() const
{
	return _code >= 300 && _code <= 399;
}
//----------------------------------------------------------------------
bool SmtpReply::isWelcome() const
{
	return _code == 220;
}
//----------------------------------------------------------------------
bool SmtpReply::isIntermediateMailData() const
{
	return _code == 354;
}
//----------------------------------------------------------------------
bool SmtpReply::isEndOfSession() const
{
	return _code == 221;
}
//----------------------------------------------------------------------


