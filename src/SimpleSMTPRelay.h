

#ifndef SIMPLESMTPRELAY_H_
#define SIMPLESMTPRELAY_H_

#include <boost/asio.hpp>

#include "SmtpReceiver.h"
#include "SmtpSender.h"


class SimpleSmtpRelay
{
public:
	SimpleSmtpRelay(boost::asio::io_service& ioService);
	void handleStop();

private:
	SimpleSmtpRelay(const SimpleSmtpRelay&);
	SimpleSmtpRelay& operator=(const SimpleSmtpRelay&);

private:
	boost::asio::io_service& _ioService;
	SmtpReceiver _receiver;
	SmtpSender _sender;
};


#endif /* SIMPLESMTPRELAY_H_ */
