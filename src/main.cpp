
#include <string>
#include <vector>
#include <syslog.h>
#include <unistd.h>

#include <boost/asio/signal_set.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>

#include "SimpleSMTPRelay.h"

//----------------------------------------------------------------------
int main()
{
	boost::asio::io_service ioService;
	std::size_t threadPoolSize = 20;

	try {
		boost::asio::signal_set signals(ioService, SIGINT, SIGTERM);

		ioService.notify_fork(boost::asio::io_service::fork_prepare);

		if(pid_t pid = fork()) {
			if(pid > 0) {
				exit(0);
			} else {
				syslog(LOG_ERR|LOG_USER, "First fork failed: %m");
				return 1;
			}
		}

		setsid();
		chdir("/");
		umask(0);

		if(pid_t pid = fork()) {
			if(pid > 0) {
				exit(0);
			} else {
				syslog(LOG_ERR|LOG_USER, "Second fork failed: %m");
				return 1;
			}
		}
		close(0);
		close(1);
		close(2);

		if(open("/dev/null", O_RDONLY) < 0) {
			syslog(LOG_ERR | LOG_USER, "Unable to open /dev/null: %m");
			return 1;
		}

		ioService.notify_fork(boost::asio::io_service::fork_child);

		syslog(LOG_INFO|LOG_USER, "Daemon started.");

		SimpleSmtpRelay simpleSmtpRelay(ioService);
		signals.async_wait(boost::bind(&SimpleSmtpRelay::handleStop, &simpleSmtpRelay));

		std::vector< boost::shared_ptr<boost::thread> > threads;

		for(std::size_t i = 0; i < threadPoolSize; i++) {
			boost::shared_ptr<boost::thread> thread (new boost::thread(
					boost::bind(&boost::asio::io_service::run, &ioService))
			);
			threads.push_back(thread);
		}

		for(std::size_t i = 0; i < threadPoolSize; i++) {
			threads[i]->join();
		}

		syslog(LOG_INFO | LOG_USER, "Daemon stopped.");

	} catch (std::exception& e) {
		syslog(LOG_ERR | LOG_USER, "Exception: %s", e.what());
	}
}
//----------------------------------------------------------------------

