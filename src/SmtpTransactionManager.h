
#ifndef SMTPTRANSACTIONMANAGER_H_
#define SMTPTRANSACTIONMANAGER_H_

#include <list>


#include <boost/atomic.hpp>
#include <boost/thread/mutex.hpp>

#include "SmtpReceiverTransaction.h"

class SmtpTransactionManager
{
public:

	static SmtpTransactionManager& getInstance();

	void add(SmtpReceiverTransactionPtr transaction);
	bool hasNext();
	SmtpReceiverTransactionPtr getNext();

	void save() {}
	void restore() {}

	virtual ~SmtpTransactionManager();

private:
	SmtpTransactionManager();
	boost::mutex _readMutex;
	std::list<SmtpReceiverTransactionPtr> _transactions;

	static boost::atomic<SmtpTransactionManager*> _instance;
	static boost::mutex _instantiationMutex;

};

#endif /* SMTPTRANSACTIONMANAGER_H_ */


