
#ifndef SMTPSENDERSESSION_H_
#define SMTPSENDERSESSION_H_

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>

#include "SmtpSenderTransaction.h"
#include "SenderStateMachine.h"
#include "MxLookup.h"

using boost::asio::ip::tcp;

class SmtpSenderSession;
typedef boost::shared_ptr<SmtpSenderSession> SmtpSenderSessionPtr;

class SmtpSenderSession
	: public boost::enable_shared_from_this<SmtpSenderSession>
{
public:
	static SmtpSenderSessionPtr create(boost::asio::io_service& ioService, SmtpSenderTransactionPtr transaction);
	virtual ~SmtpSenderSession();
	tcp::socket& getSocket();
	void start();

private:
	SmtpSenderSession(boost::asio::io_service& ioService, SmtpSenderTransactionPtr transaction);

	void waitWelcomeMessage();
	void onResolve(const boost::system::error_code& error, tcp::resolver::iterator endpointIterator);
	void onConnect(const boost::system::error_code& error);
	void onRead(const boost::system::error_code& error);
	void onWrite(const boost::system::error_code& error);
	void waitForReply();

	std::string getHelloCommand() const;
	std::string getMailCommand() const;
	std::string getRcptCommand() const;
	std::string getDataCommand() const;
	std::string getMailDataCommand() const;
	std::string getEndOfSessionCommand() const;

	void tryNextMxCandidate();

	std::string getNextCommand();
	bool isTerminated() const;
	bool isError() const;

private:
	tcp::socket _socket;
	tcp::resolver _resolver;
	boost::asio::io_service::strand _strand;

	boost::asio::streambuf _reply;

	MxEntries _mxCandidates;

	SmtpSenderTransactionPtr _transaction;
	SenderStateMachine _stateMachine;
};

#endif /* SMTPSENDERSESSION_H_ */
