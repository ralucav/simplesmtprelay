
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <resolv.h>

#include <iostream>
#include <vector>

#include "MxLookup.h"

//----------------------------------------------------------------------
MxLookup::MxLookup() {
}
//----------------------------------------------------------------------
MxLookup::~MxLookup() {
}
//----------------------------------------------------------------------
// courtesy of Stackoverflow
MxEntries MxLookup::lookup(const std::string& domainName)
{
	u_char nsbuf[4096];
	char dispbuf[4096];
	ns_msg msg;
	ns_rr rr;
	int j, l;

	MxEntries mxEntries;

	l = res_query (domainName.c_str(), ns_c_any, ns_t_mx, nsbuf, sizeof (nsbuf));
	if (l < 0) {
		//TODO: error, send delivery failure?
	} else {
		ns_initparse (nsbuf, l, &msg);

		l = ns_msg_count (msg, ns_s_an);

	    for (j = 0; j < l; j++) {
	    	ns_parserr (&msg, ns_s_an, j, &rr);
	        ns_sprintrr (&msg, &rr, NULL, NULL, dispbuf, sizeof (dispbuf));

	        mxEntries.push_back(getMxEntryName((char*)dispbuf));
	    }
	}
	return mxEntries;
}
//----------------------------------------------------------------------
std::string MxLookup::getMxEntryName(char* mxRecord)
{
	std::string mxRecordStr(mxRecord);
	int lastSpacePosition = mxRecordStr.find_last_of(' ');
	int mxRecordLength = mxRecordStr.length();
	std::string serverName = mxRecordStr.substr(lastSpacePosition + 1, (mxRecordLength - 1) - lastSpacePosition - 1);
	return serverName;
}
//----------------------------------------------------------------------




