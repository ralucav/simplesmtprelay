
#ifndef SMTPSERVER_H_
#define SMTPRECEIVER_H_

#include <boost/asio.hpp>
#include "SmtpReceiverSession.h"

using boost::asio::ip::tcp;

class SmtpReceiver
{
public:
	SmtpReceiver(boost::asio::io_service& ioService);
	virtual ~SmtpReceiver(){}

private:
	SmtpReceiver(const SmtpReceiver&);
	SmtpReceiver& operator=(const SmtpReceiver&);

	void startAccept();
	void onAccept(SmtpReceiverSessionPtr smtpSession, const boost::system::error_code& error);

private:
	boost::asio::io_service& _ioService;
	tcp::acceptor _acceptor;
};

#endif /* SMTPSERVER_H_ */
