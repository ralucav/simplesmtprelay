
#include <syslog.h>
#include "SimpleSMTPRelay.h"

//----------------------------------------------------------------------
SimpleSmtpRelay::SimpleSmtpRelay(boost::asio::io_service& ioService)
	 : _ioService(ioService),
	   _receiver(ioService),
	   _sender(ioService)
{
}
//----------------------------------------------------------------------
void SimpleSmtpRelay::handleStop()
{
	syslog(LOG_INFO, "handle stop signal");
	_ioService.stop();
}
//----------------------------------------------------------------------
