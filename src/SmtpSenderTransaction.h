
#ifndef SMTPSENDERTRANSACTION_H_
#define SMTPSENDERTRANSACTION_H_

#include <string>
#include <list>

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "SmtpReceiverTransaction.h"

class SmtpSenderTransaction;
typedef boost::shared_ptr<SmtpSenderTransaction> SmtpSenderTransactionPtr;
typedef std::list<SmtpSenderTransactionPtr> SmtpSenderTransactions;

class SmtpSenderTransaction
	: public boost::enable_shared_from_this<SmtpSenderTransaction>
{
public:

	static SmtpSenderTransactionPtr create(const std::string& mailFrom, const std::string& rcptTo, SmtpReceiverTransactionPtr receiverTransaction);

	virtual ~SmtpSenderTransaction();

	std::string& getMailFrom() { return _mailFrom; }
	std::string& getRecipient() { return _rcptTo; }
	std::string& getRecipientDomain() { return _rcptDomainName; }
	std::string& getMailData() { return _receiverTransaction->getDataContent(); }

private:
	SmtpSenderTransaction(const std::string& mailFrom, const std::string& rcptTo, SmtpReceiverTransactionPtr receiverTransaction);
	std::string domainName(const std::string& address);

private:
	std::string _mailFrom;
	std::string _rcptTo;
	std::string _rcptDomainName;
	SmtpReceiverTransactionPtr _receiverTransaction;
};

#endif /* SMTPSENDERTRANSACTION_H_ */
