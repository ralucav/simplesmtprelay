
#ifndef SMTPREPLIES_H_
#define SMTPREPLIES_H_

#include "SmtpReply.h"

class SmtpReplies
{
public:
	typedef enum {
		COMMAND_UNRECOGNIZED = 0, 			// 500
		SYNTAX_ERROR_PARAMS_ARGUMENTS,		// 501
		COMMAND_NOT_IMPLEMENTED,			// 502
		BAD_SEQUENCE_OF_COMMANDS,			// 503
		SERVICE_READY,						// 220
		SERVICE_CLOSING,					// 221
		SERVICE_NOT_AVAILABLE,				// 421
		REQUESTED_ACTION_COMPLETED,			// 250
		ENTER_MAIL_DATA,					// 354
		LAST_REPLY
	} ReplyEnum;

public:
	static SmtpReplies& getInstance();
	virtual ~SmtpReplies();
	SmtpReply& getReply(ReplyEnum reply);

private:
	static SmtpReplies* _instance;

	SmtpReplies();
	SmtpReply _replies [LAST_REPLY];
};

#endif /* SMTPREPLIES_H_ */

