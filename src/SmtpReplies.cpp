
#include "SmtpReplies.h"

SmtpReplies* SmtpReplies::_instance = 0;

//----------------------------------------------------------------------
SmtpReplies::SmtpReplies() {
	_replies[SERVICE_READY] = SmtpReply(220, "Ready to serve!");
	_replies[SERVICE_CLOSING] = SmtpReply(221, "Service closing!");
	_replies[REQUESTED_ACTION_COMPLETED] = SmtpReply(250, "Action completed!");
	_replies[ENTER_MAIL_DATA] = SmtpReply(354, "Enter mail data!");
}
//----------------------------------------------------------------------
SmtpReplies::~SmtpReplies() {
}
//----------------------------------------------------------------------
SmtpReplies& SmtpReplies::getInstance()
{
	if (_instance == 0) {
		_instance = new SmtpReplies();
	}
	return *_instance;
}
//----------------------------------------------------------------------
SmtpReply& SmtpReplies::getReply(ReplyEnum reply)
{
	return _replies[reply];
}
//----------------------------------------------------------------------
