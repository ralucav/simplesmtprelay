
#ifndef SMTPCOMMAND_H_
#define SMTPCOMMAND_H_

#include <string>
#include <list>

class SmtpCommand
{
public:
	typedef enum {
		HELO = 0,
		EHLO,
		MAIL,
		RCPT,
		DATA,
		QUIT,
		UNKNOWN_COMMAND,
		LAST_COMMAND
	} Verb;

public:
	SmtpCommand(const std::string& command, const std::string& content);
	SmtpCommand(const std::string& command);

	SmtpCommand(Verb verb, const std::string& content);
	SmtpCommand(Verb verb);
	SmtpCommand (const SmtpCommand& other);
	virtual ~SmtpCommand(){}

	Verb getVerb() { return _verb; }
	std::string& getContent() { return _content; }

	void setContent(const std::string& content);
	bool isUnknown() {return _verb == UNKNOWN_COMMAND; }

	bool isMail() const { return _verb == MAIL; }
	bool isRcpt() const { return _verb == RCPT; }
	bool isData() const { return _verb == DATA; }

	bool isMailTransactionCommand() const;

	static const std::string& getCommandName(Verb verb);

private:
	Verb getVerb(const std::string& command);

private:
	Verb _verb;
	std::string _content;

	static const std::string COMMAND_NAME[LAST_COMMAND];
};

typedef std::list<SmtpCommand> SmtpCommands;

#endif /* SMTPCOMMAND_H_ */
