
#include <boost/date_time/posix_time/posix_time.hpp>
#include <syslog.h>

#include "SmtpSender.h"
#include "SmtpCommand.h"
#include "SmtpTransactionManager.h"
#include "SmtpReceiverTransaction.h"
#include "SmtpSenderTransaction.h"
#include "SmtpSenderSession.h"

//----------------------------------------------------------------------
SmtpSender::SmtpSender(boost::asio::io_service& ioService)
	: _ioService(ioService)
{
	_internalThread = boost::thread(&SmtpSender::processTransactionQueue, this);
}
//----------------------------------------------------------------------
SmtpSender::~SmtpSender() {
	_internalThread.interrupt();
	_internalThread.join();
}
//----------------------------------------------------------------------
void SmtpSender::processTransactionQueue()
{
	try {
		while(1) {
			boost::this_thread::sleep(boost::posix_time::milliseconds(2000));

			if(SmtpTransactionManager::getInstance().hasNext()) {
				SmtpReceiverTransactionPtr transaction = SmtpTransactionManager::getInstance().getNext();
				SmtpSenderTransactions senderTransactions = getSenderTransactions(transaction);

				for(SmtpSenderTransactions::iterator it = senderTransactions.begin(); it != senderTransactions.end(); it ++) {
					SmtpSenderTransactionPtr t = *it;
					SmtpSenderSessionPtr senderSession = SmtpSenderSession::create(_ioService, t);
					senderSession->start();
				}
			}
		}

	} catch (boost::thread_interrupted& interruption) {

	} catch (std::exception& e) {

	}
}
//----------------------------------------------------------------------
SmtpSenderTransactions SmtpSender::getSenderTransactions(SmtpReceiverTransactionPtr receiverTransaction)
{
	SmtpSenderTransactions senderTransactions;
	std::string mailFrom;

	SmtpCommands commands = receiverTransaction->getCommands();
	for(SmtpCommands::iterator it = commands.begin(); it != commands.end(); it ++) {
		SmtpCommand c = *it;

		if( c.isMail() ) {
			mailFrom = getAddress(c.getContent());
		} else if( c.isRcpt() ) {
			std::string rcptTo = getAddress(c.getContent());
			SmtpSenderTransactionPtr senderTransaction = SmtpSenderTransaction::create(mailFrom, rcptTo, receiverTransaction);
			senderTransactions.push_back(senderTransaction);
		}
	}

	return senderTransactions;
}
//----------------------------------------------------------------------
std::string SmtpSender::getAddress(const std::string& commandContent)
{
	int first = commandContent.find('<');
	int last = commandContent.find('>');
	return commandContent.substr(first + 1, last - first - 1);
}
//----------------------------------------------------------------------
void SmtpSender::printSenderTransactions(const SmtpSenderTransactions& senderTransactions)
{
	for(SmtpSenderTransactions::const_iterator it = senderTransactions.begin(); it != senderTransactions.end(); it ++) {
		SmtpSenderTransactionPtr t = *it;

		std::cout << "[printSenderTransactions]: " << std::endl << std::flush;
		std::cout << t->getMailFrom() << " -> " << t->getRecipient() << " [" << t->getRecipientDomain() << "]" << std::endl << std::flush;
		std::cout << t->getMailData() << std::endl << std::flush;
	}
}

