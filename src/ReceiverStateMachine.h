
#ifndef RECEIVERSTATEMACHINE_H_
#define RECEIVERSTATEMACHINE_H_

#include "SmtpCommand.h"
#include "SmtpReplies.h"

class ReceiverStateMachine
{
	typedef enum {
		INITIAL = 0,
		HELO,
		MAIL,
		RCPT,
		DATA,
		END_OF_SESSION,
		LAST_STATE
	} ReceiverState;

public:
	ReceiverStateMachine();

	bool isEndOfSession() const {
		return _state == END_OF_SESSION;
	}

	bool isMailData() const {
		return _state == DATA;
	}

	bool isMail() const {
		return _state == MAIL;
	}

	bool isHelo() const {
		return _state == HELO;
	}

	SmtpReply& advanceState(SmtpCommand command);
	SmtpReply& advanceToHelo();

	static const std::string& getStateName(ReceiverState stat);

	virtual ~ReceiverStateMachine();

private:
	SmtpReply& badSequenceOfCommands() const;

	static const std::string STATE_NAME[LAST_STATE];

private:
	ReceiverState _state;
};

#endif /* RECEIVERSTATEMACHINE_H_ */
