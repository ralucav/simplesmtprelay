
#ifndef MXLOOKUP_H_
#define MXLOOKUP_H_

#include <string>
#include <list>

typedef std::list<std::string> MxEntries;

class MxLookup
{
public:
	virtual ~MxLookup();
	static MxEntries lookup(const std::string& domainName);

private:
	MxLookup();
	static std::string getMxEntryName(char* mxRecord);
};

#endif /* MXLOOKUP_H_ */
