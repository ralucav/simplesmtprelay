
#include <iostream>
#include <syslog.h>
#include "ReceiverStateMachine.h"

const std::string ReceiverStateMachine::STATE_NAME [ReceiverStateMachine::LAST_STATE] = {
		"INITIAL_STATE", "HELO_STATE", "MAIL_STATE", "RCPT_STATE", "DATA_STATE", "END_OF_SESSION_STATE"
};

//----------------------------------------------------------------------
ReceiverStateMachine::ReceiverStateMachine() {
	_state = INITIAL;
}
//----------------------------------------------------------------------
ReceiverStateMachine::~ReceiverStateMachine() {
}
//----------------------------------------------------------------------
SmtpReply& ReceiverStateMachine::advanceState(SmtpCommand command)
{
	const std::string& commandName = SmtpCommand::getCommandName(command.getVerb());
	syslog(LOG_INFO, "Receiver state machine state(%s) command(%s)", getStateName(_state).c_str(), commandName.c_str());

	if(command.getVerb() == SmtpCommand::QUIT) {
		_state = END_OF_SESSION;
		return SmtpReplies::getInstance().getReply(SmtpReplies::SERVICE_CLOSING);
	}

	switch(_state) {
	case INITIAL:
		if((command.getVerb() != SmtpCommand::HELO) && (command.getVerb() != SmtpCommand::EHLO)) {
			return badSequenceOfCommands();
		}
		_state = HELO;
		break;
	case HELO:
		if((command.getVerb() != SmtpCommand::MAIL)) {
			return badSequenceOfCommands();
		}
		_state = MAIL;
		break;
	case MAIL:
		if((command.getVerb() != SmtpCommand::RCPT)) {
			return badSequenceOfCommands();
		}
		_state = RCPT;
		break;
	case RCPT:
		if(command.getVerb() == SmtpCommand::RCPT) {
			break;
		}
		if((command.getVerb() != SmtpCommand::DATA)) {
			return badSequenceOfCommands();
		}
		_state = DATA;
		return SmtpReplies::getInstance().getReply(SmtpReplies::ENTER_MAIL_DATA);
		// break;
	case DATA:
		return badSequenceOfCommands();
		// break;
	case END_OF_SESSION:
		return badSequenceOfCommands();
		// break;
	default:
		return SmtpReplies::getInstance().getReply(SmtpReplies::COMMAND_UNRECOGNIZED);
		// break;
	}
	return SmtpReplies::getInstance().getReply(SmtpReplies::REQUESTED_ACTION_COMPLETED);
}
//----------------------------------------------------------------------
SmtpReply& ReceiverStateMachine::badSequenceOfCommands() const
{
	return SmtpReplies::getInstance().getReply(SmtpReplies::BAD_SEQUENCE_OF_COMMANDS);
}
//----------------------------------------------------------------------
SmtpReply& ReceiverStateMachine::advanceToHelo()
{
	// TODO: _state == DATA?
	_state = HELO;
	return SmtpReplies::getInstance().getReply(SmtpReplies::REQUESTED_ACTION_COMPLETED);
}
//----------------------------------------------------------------------
const std::string& ReceiverStateMachine::getStateName(ReceiverStateMachine::ReceiverState state)
{
	return STATE_NAME[state];
}
//----------------------------------------------------------------------



