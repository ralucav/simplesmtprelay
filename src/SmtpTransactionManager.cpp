
#include "SmtpTransactionManager.h"

 boost::atomic<SmtpTransactionManager*> SmtpTransactionManager::_instance(0);
 boost::mutex SmtpTransactionManager::_instantiationMutex;

//----------------------------------------------------------------------
SmtpTransactionManager::SmtpTransactionManager() {
}
//----------------------------------------------------------------------
SmtpTransactionManager::~SmtpTransactionManager() {
}
//----------------------------------------------------------------------
SmtpTransactionManager& SmtpTransactionManager::getInstance()
{
	SmtpTransactionManager* tmp = _instance.load(boost::memory_order_consume);
    if (!tmp) {
      boost::mutex::scoped_lock lock(_instantiationMutex);
      tmp = _instance.load(boost::memory_order_consume);
      if (!tmp) {
        tmp = new SmtpTransactionManager();
        _instance.store(tmp, boost::memory_order_release);
      }
    }
    return *tmp;
}
//----------------------------------------------------------------------
void SmtpTransactionManager::add(SmtpReceiverTransactionPtr transaction)
{
	boost::mutex::scoped_lock lock(_readMutex);
	_transactions.push_back(transaction);
}
//----------------------------------------------------------------------
bool SmtpTransactionManager::hasNext()
{
	boost::mutex::scoped_lock lock(_readMutex);
	return !_transactions.empty();
}
//----------------------------------------------------------------------
SmtpReceiverTransactionPtr SmtpTransactionManager::getNext()
{
	// assert not empty
	boost::mutex::scoped_lock(_readMutex);
	SmtpReceiverTransactionPtr transaction = _transactions.front();
	_transactions.pop_front();
	return transaction;
}
//----------------------------------------------------------------------
