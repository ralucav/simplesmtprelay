
#include "SmtpSenderTransaction.h"

//----------------------------------------------------------------------
SmtpSenderTransaction::SmtpSenderTransaction(const std::string& mailFrom, const std::string& rcptTo, SmtpReceiverTransactionPtr receiverTransaction)
	: _mailFrom(mailFrom), _rcptTo(rcptTo), _receiverTransaction(receiverTransaction)
{
	_rcptDomainName = domainName(_rcptTo);
}
//----------------------------------------------------------------------
SmtpSenderTransaction::~SmtpSenderTransaction()
{
}
//----------------------------------------------------------------------
SmtpSenderTransactionPtr SmtpSenderTransaction::create(const std::string& mailFrom, const std::string& rcptTo, SmtpReceiverTransactionPtr receiverTransaction)
{
	return SmtpSenderTransactionPtr(new SmtpSenderTransaction(mailFrom, rcptTo, receiverTransaction));
}
//----------------------------------------------------------------------
std::string SmtpSenderTransaction::domainName(const std::string& address)
{
	int pos = address.find('@');
	return address.substr(pos+1);
}
//----------------------------------------------------------------------
