
#include <iostream>
#include <istream>
#include <sstream>
#include <syslog.h>
#include <boost/bind.hpp>
#include "SmtpReceiverSession.h"
#include "SmtpTransactionManager.h"
#include "SmtpConstants.h"

//----------------------------------------------------------------------
SmtpReceiverSession::SmtpReceiverSession(boost::asio::io_service& ioService)
	: _socket(ioService), _strand(ioService), _command("", "")
{
}
//----------------------------------------------------------------------
SmtpReceiverSession::~SmtpReceiverSession() {}
//----------------------------------------------------------------------
SmtpReceiverSessionPtr SmtpReceiverSession::create(boost::asio::io_service& ioService)
{
	return SmtpReceiverSessionPtr(new SmtpReceiverSession(ioService));
}
//----------------------------------------------------------------------
void SmtpReceiverSession::start()
{
	syslog(LOG_INFO, "SmtpReceiverSession started");
	sendWelcomeMessage();
}
//----------------------------------------------------------------------
void SmtpReceiverSession::onWrite(const boost::system::error_code& error)
{
	if(!error) {
		if(!isEndOfSession()) {
			waitForData();
		}
	} else {
		syslog(LOG_WARNING, "Receiver error on write: %s", error.message().c_str());
	}
}
//----------------------------------------------------------------------
void SmtpReceiverSession::onSimpleCommand(const boost::system::error_code& error, size_t bytesTransferred)
{
	if(!error) {
		std::istream commandStream(&_request);
		std::string commandVerb;
		commandStream >> commandVerb;

		std::string commandContent;
		std::getline(commandStream, commandContent);

		syslog(LOG_INFO, "Receive command %s [%zd bytes] [%s]", commandVerb.c_str(), commandContent.length(), commandContent.c_str());

		_command = SmtpCommand(commandVerb, commandContent);
		SmtpReply& reply = _stateMachine.advanceState(_command);

		if(beginTransaction(_command, reply)) {
			syslog(LOG_INFO, "Begin transaction");
			_transaction = SmtpReceiverTransaction::create();
		}

		if(addCommandToTransaction(_command, reply)) {
			syslog(LOG_INFO, "Add command to current transaction");
			_transaction->addCommand(_command);
		}

		sendReply(reply);

	} else {
		syslog(LOG_WARNING, "Receiver error on simple command: %s", error.message().c_str());
	}
}
//----------------------------------------------------------------------
bool SmtpReceiverSession::addCommandToTransaction(const SmtpCommand& command, const SmtpReply& reply) const
{
	// don't add the DATA command to transaction just yet
	// it will be added after the mail data is received, upon CRLF.CRLF
	if(!reply.isSuccessOrIntermediate()) return false;
	if(!command.isMailTransactionCommand()) return false;
	return !isMailData();
}
//----------------------------------------------------------------------
bool SmtpReceiverSession::beginTransaction(const SmtpCommand& command, SmtpReply& reply) const
{
	return command.isMail() && reply.isActionCompleted();
}
//----------------------------------------------------------------------
void SmtpReceiverSession::onMailData(const boost::system::error_code& error, size_t bytesTransferred)
{
	if(!error) {
		boost::asio::streambuf::const_buffers_type bufs = _request.data();
		std::string dataContent(
		    boost::asio::buffers_begin(bufs),
		    boost::asio::buffers_begin(bufs) + bytesTransferred);

		_request.consume(bytesTransferred);

		syslog(LOG_INFO, "Receive mail data %zd bytes", bytesTransferred);

		_command.setContent(dataContent);
		_transaction->addCommand(_command);
		SmtpTransactionManager::getInstance().add(_transaction);

		SmtpReply& reply = _stateMachine.advanceToHelo();
		sendReply(reply);
	} else {
		syslog(LOG_WARNING, "Receiver error on mail data: %s", error.message().c_str());
	}
}
//----------------------------------------------------------------------
void SmtpReceiverSession::sendWelcomeMessage()
{
	SmtpReply& welcomeReply = SmtpReplies::getInstance().getReply(SmtpReplies::SERVICE_READY);
	sendReply(welcomeReply);
}
//----------------------------------------------------------------------
void SmtpReceiverSession::waitForData()
{
	std::string delimiter;
	void (SmtpReceiverSession::*readCallback) (const boost::system::error_code&, size_t);

	if(isMailData()) {
		readCallback = &SmtpReceiverSession::onMailData;
		delimiter = SmtpConstants::MAIL_DATA_DELIMITER;
	} else {
		readCallback = &SmtpReceiverSession::onSimpleCommand;
		delimiter = SmtpConstants::DELIMITER;
	}

	boost::asio::async_read_until(
		_socket,
		_request, delimiter,
		_strand.wrap(boost::bind(
				readCallback,
				shared_from_this(),
				boost::asio::placeholders::error,
				boost::asio::placeholders::bytes_transferred))

	);
}//----------------------------------------------------------------------
void SmtpReceiverSession::sendReply(SmtpReply& reply)
{
	syslog(LOG_INFO, "Send reply %d [%s]", reply.getCode(), reply.getContent().c_str());

	std::stringstream replyStream;
	replyStream << reply.getCode() << " ";
	replyStream << reply.getContent() << SmtpConstants::DELIMITER;

	boost::asio::async_write(
		_socket,
		boost::asio::buffer(replyStream.str()),
		_strand.wrap(boost::bind(
				&SmtpReceiverSession::onWrite,
				shared_from_this(),
				boost::asio::placeholders::error))
	);
}
//----------------------------------------------------------------------
tcp::socket& SmtpReceiverSession::getSocket()
{
	return _socket;
}
//----------------------------------------------------------------------
