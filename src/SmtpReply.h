
#ifndef SMTPREPLY_H_
#define SMTPREPLY_H_

#include <string>

class SmtpReply
{
public:
	SmtpReply(unsigned short int code, const std::string& content);
	SmtpReply(unsigned short int code);
	SmtpReply(const SmtpReply& other);
	SmtpReply();

	bool isActionCompleted() const;
	bool isWelcome() const;
	bool isIntermediateMailData() const;
	bool isEndOfSession() const;

	bool isSuccessOrIntermediate() const;
	bool isSuccess() const;
	bool isIntermediate() const;

	unsigned short int getCode() const { return _code; }
	std::string& getContent()  { return _content; }

	virtual ~SmtpReply(){}
private:

private:
	unsigned short int _code;
	std::string _content;
};

#endif /* SMTPREPLY_H_ */
