
#include "SmtpCommand.h"

const std::string SmtpCommand::COMMAND_NAME[SmtpCommand::LAST_COMMAND] = {
		"HELO", "EHLO", "MAIL", "RCPT", "DATA", "QUIT", "UNKNOWN_COMMAND"
};

//----------------------------------------------------------------------
SmtpCommand::SmtpCommand(Verb verb, const std::string& content)
	: _verb(verb), _content(content)
{
}
//----------------------------------------------------------------------
SmtpCommand::SmtpCommand(Verb verb)
 : _verb(verb), _content("")
{
}
//----------------------------------------------------------------------
SmtpCommand::SmtpCommand(const std::string& command, const std::string& content)
{
	_verb = getVerb(command);
	_content = content;
}
//----------------------------------------------------------------------
SmtpCommand::SmtpCommand(const std::string& command)
{
	_verb = getVerb(command);
}
//----------------------------------------------------------------------
SmtpCommand::SmtpCommand(const SmtpCommand& other)
{
	_verb = other._verb;
	_content = other._content;
}
//----------------------------------------------------------------------
void SmtpCommand::setContent(const std::string& content)
{
	_content = content;
}
//----------------------------------------------------------------------
SmtpCommand::Verb SmtpCommand::getVerb(const std::string& command)
{
	if(command == "HELO") return HELO;
	if(command == "EHLO") return EHLO;
	if(command == "MAIL") return MAIL;
	if(command == "RCPT") return RCPT;
	if(command == "DATA") return DATA;
	if(command == "QUIT") return QUIT;
	return UNKNOWN_COMMAND;
}
//----------------------------------------------------------------------
bool SmtpCommand::isMailTransactionCommand() const
{
	return _verb == MAIL || _verb == RCPT || _verb == DATA;
}
//----------------------------------------------------------------------
const std::string& SmtpCommand::getCommandName(SmtpCommand::Verb verb)
{
	return COMMAND_NAME[verb];
}
//----------------------------------------------------------------------

