
#ifndef SMTPTRECEIVERRANSACTION_H_
#define SMTPTRECEIVERRANSACTION_H_


#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/noncopyable.hpp>

#include "SmtpCommand.h"

class SmtpReceiverTransaction;
typedef boost::shared_ptr<SmtpReceiverTransaction> SmtpReceiverTransactionPtr;


class SmtpReceiverTransaction
	: public boost::enable_shared_from_this<SmtpReceiverTransaction>,
	  private boost::noncopyable
{
public:

	static SmtpReceiverTransactionPtr create()
	{
		return SmtpReceiverTransactionPtr(new SmtpReceiverTransaction());
	}

	virtual ~SmtpReceiverTransaction(){}
	void addCommand(SmtpCommand command)
	{
		_commands.push_back(command);
		if(command.isData()) {
			_dataCommand = command;
		}
	}

	const SmtpCommands& getCommands()
	{
		return _commands;
	}

	std::string& getDataContent() {
		return _dataCommand.getContent();
	}

private:
	SmtpReceiverTransaction() : _dataCommand("", ""){}
	SmtpCommands _commands;
	SmtpCommand _dataCommand;
};

#endif /* SMTPTRANSACTION_H_ */
