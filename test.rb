#!/usr/bin/ruby

require 'thread'

def sendMail (numThread)
	mailText = "This is an email sent by thread #{numThread}"

	sleep(rand(0)/10.0)
	output = %x[ echo #{mailText} | mailx -S smtp=127.0.0.1 -S from=ralucav@gmail.com -v -s "simple smtp relay test" ralucav@gmail.com ]
end

threadPool = []
10.times do |t|
	threadPool[t] = Thread.new{sendMail(t)}
end

threadPool.each { |t| t.join; }


