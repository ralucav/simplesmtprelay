TARGET = SimpleSMTPRelay
LIBS = -lboost_system -lboost_thread -lresolv
CXX = g++
CXXFLAGS = -O3 -Wall

.PHONY: all clean

SOURCES = $(wildcard */*.cpp)
HEADERS = $(wildcard */*.h)
OBJECTS = $(patsubst %.cpp, %.o, $(SOURCES))

all: $(TARGET)

%.o:%.cpp $(HEADERS)
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(TARGET): $(OBJECTS)
	$(CXX) $(OBJECTS) -Wall $(LIBS) -o $@

clean:
	rm -f $(OBJECTS)
	rm -f $(TARGET)
